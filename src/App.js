import React, { useState, useEffect } from 'react';
import axios from "axios"
import './App.css';
// add component
import Navbar from './components/Layout/Navbar'
import Users from './components/Users/Users'
import UserItem from './components/Users/UserItem'
function App() {

  const [user, setUser] = useState([])
  const [loading, setLoading] = useState(false)
  // chon mikham update konm useramo az useEffect estefade mikonm

  useEffect(async () => {
    setLoading(true)
    // inja az axios estefade mikonm vase ertebat ba api / res pasokh az samt aoi ro dar khodesh zakhire mikone  dar vaghe res objecti hast k bkham dastresi dashte bashm b maghadire dakhlesh bayad az data stefade konm

    const res = await axios.get("https://api.github.com/users")

    // console.log(res.data)

    setUser(res.data)

    // vaghti etelaat kaml omd setloading ro false mikonm

    setLoading(false)
  }, [])
  // chon hhtp requst mifrestm async mikonm
  const serchUsers = async (text) => {
    const res = await axios.get()
    setUser(text)
  }
  return (
    <>
      <Navbar />

      <div className="container mx-auto">
        {/* props tarif mikonm vase Users k users hasho az dakhle const[user,setuser] migire */}


        <Users users={user} loading={loading} />
      </div>
    </>
  );
}

export default App;
