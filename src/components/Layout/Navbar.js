import React from 'react';

import Serch from './Serch'

// chon state mn array az object has dakhle[ ] gharar midahimesh


export default function Navbar() {
    return (
        <>
            <nav className="navbar navbar-dark bg-dark py-3">
                <div className="container">
                    <a href="/" className="navbar-brand">
                        <i className="fab fa-github-alt mx-2"></i>
                        Github Find
                    </a>
                    <Serch />
                </div>
            </nav>
        </>
    )
}
