import React from 'react'
import spinner from "./spinner.gif"
export default function Spinners() {
  return (
    <>
      <img src={spinner} alt="spinnerLoading"/>
    </>
  )
}
