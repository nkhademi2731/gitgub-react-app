import React from 'react'

export default function UserItem(props) {
    // property hay hamname user dakhle in moteghayera gharar bgire
    const { html_url, avatar_url, login } = props.user
    return (
        <>
            <div className="col-4 p-2" >
                <div className="card">
                    <div className="card-body text-center">
                        <img src={avatar_url} className="rounded-circle " style={{width:"60px"}} />
                        <h3 className="my-2">{login}</h3>
                        <a href={html_url} className="card-link btn btn-primary my-3">More</a>
                    </div>
                </div>
            </div>
        </>
    )
}
