import React from 'react'
import UserItem from "./UserItem"
import Spinners from "../Layout/Spinners"
export default function Users(props) {
    // b users haei k az api omde dastresi daram
    const { users, loading } = props
    if (!loading) {
        return (
            <>
                <div className="row my-5">

                    {users && users.map(user => (<UserItem key={user.id} user={user} />))}
                </div>
            </>
        )
    } else {
        return <div className="text-center">
            <Spinners />
        </div>
    }
}
